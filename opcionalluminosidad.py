import sys
import images as im


def brillo(image: dict, change: float) -> dict:
    width, height = im.size(image)
    new_image = im.create_blank(width, height)

    for i in range(width * height):
        if change > 0:
            pixel = image['pixels'][i]
            original_r = pixel[0]
            original_g = pixel[1]
            original_b = pixel[2]
            brillo_r = int(original_r * change)
            brillo_g = int(original_g * change)
            brillo_b = int(original_b * change)
            pixel_brillo = (brillo_r, brillo_g, brillo_b)
            pixel_brillo_lista = list(pixel_brillo)
            for x in range(len(pixel_brillo)):
                if pixel_brillo_lista[x] < 0:
                    pixel_brillo_lista[x] = 0
                elif pixel_brillo_lista[x] > 255:
                    pixel_brillo_lista[x] = 255
            new_image['pixels'][i] = pixel_brillo

        elif change < 0:
            pixel = image['pixels'][i]
            original_r = pixel[0]
            original_g = pixel[1]
            original_b = pixel[2]
            brillo_r = int(original_r // abs(change))
            brillo_g = int(original_g // abs(change))
            brillo_b = int(original_b // abs(change))
            pixel_brillo = (brillo_r, brillo_g, brillo_b)
            pixel_brillo_lista = list(pixel_brillo)
            for x in range(len(pixel_brillo)):
                if pixel_brillo_lista[x] < 0:
                    pixel_brillo_lista[x] = 0
                elif pixel_brillo_lista[x] > 255:
                    pixel_brillo_lista[x] = 255
            new_image['pixels'][i] = pixel_brillo

        else:
            new_image['pixels'][i] = image['pixels'][i]


    im.write_img(image=new_image, filename='imagen_brillo.png')
    return new_image

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Uso: opcionalluminosidad.py <archivo_imagen> <factor_brillo>")
        sys.exit(1)

    filename = sys.argv[1]
    factor_brillo = float(sys.argv[2])

    pixels = im.read_img(filename)
    new_image = brillo(pixels, factor_brillo)

    new_filename = filename.replace(".", "_brillo.")
    im.write_img(new_image, new_filename)


