import sys
from transform import *
from images import read_img, write_img, create_blank
#from transform import change_colors, rotate, shift, crop, filter
def parameters():

    image_file = sys.argv[1]
    transformation = sys.argv[2]
    return image_file, transformation
def main():
    image_file, transformation = parameters()
    image = read_img(image_file)

    if transformation == "change_colors":
        original = sys.argv[3].split(":")
        change = sys.argv[4].split(":")
        for j in range(len(original)):
            rgb_original = tuple(int(n) for n in original[j].split(","))
            rgb_change = tuple(int(n) for n in change[j].split(","))
            original[j] = rgb_original
            change[j] = rgb_change
        image_trans = change_colors(image, original, change)


    elif transformation == "rotate":
        if len(sys.argv) < 4:
            print("Usage: python3 transform_args.py <image_file> rotate <direction>")
            sys.exit(1)
        direction = sys.argv[3]
        image_trans = rotate(image, direction)

    elif transformation == "shift":
         if len(sys.argv) < 5:
            print("Usage: python3 transform_args.py <image_file> shift <horizontal> <vertical>")
            sys.exit(1)
         horizontal_shift = int(sys.argv[3])
         vertical_shift = int(sys.argv[4])
         image_trans = shift(image, horizontal_shift, vertical_shift)

    elif transformation == "crop":
        if len(sys.argv) < 7:
            print("Usage: python3 transform_args.py <image_file> crop <x> <y> <width> <height>")
            sys.exit(1)
        x = int(sys.argv[3])
        y = int(sys.argv[4])
        width = int(sys.argv[5])
        height = int(sys.argv[6])
        image_trans = crop(image, x, y, width, height)
    elif transformation == "filter":
        if len(sys.argv) < 6:
            print("Usage: python3 transform_args.py <image_file> filter <r_factor> <g_factor> <b_factor>")
            sys.exit(1)
        r_factor = float(sys.argv[3])
        g_factor = float(sys.argv[4])
        b_factor = float(sys.argv[5])
        image_trans = filter(image, r_factor, g_factor, b_factor)
    else:
        print("Transformación no válida")
        sys.exit(1)

    file_name = sys.argv[1].replace(".", "_trans.")
    write_img(image_trans, file_name)

if __name__ == '__main__':
    main()