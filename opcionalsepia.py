import sys
import images as im

def sepia(image: dict) -> dict:
    width, height = im.size(image)
    new_image = im.create_blank(width, height)

    for i in range(len(image['pixels'])):
        pixel = image['pixels'][i]
        original_r, original_g, original_b = pixel

        sepia_r = round(0.393 * original_r + 0.769 * original_g + 0.189 * original_b)
        sepia_g = round(0.349 * original_r + 0.686 * original_g + 0.168 * original_b)
        sepia_b = round(0.272 * original_r + 0.534 * original_g + 0.131 * original_b)

        sepia_r = max(0, min(sepia_r, 255))
        sepia_g = max(0, min(sepia_g, 255))
        sepia_b = max(0, min(sepia_b, 255))

        new_image['pixels'][i] = (sepia_r, sepia_g, sepia_b)

    return new_image

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Uso: opcionalsepia.py <archivo_imagen> <operacion>")
        sys.exit(1)

    filename = sys.argv[1]
    operation = sys.argv[2]

    if operation == "sepia":
        image = im.read_img(filename)
        new_image = sepia(image)
        im.write_img(new_image, sys.argv[1].replace(".", "_sepia."))

    else:
        print(f"Operación '{operation}' no válida.")

