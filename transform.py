from images import *

def mirror(image: dict) -> dict:
    width, height = size(image)
    new_image = create_blank(width, height)

    for i in range(width * height):
        x = i % width
        y = i // width
        new_x = width - x - 1
        new_pixel_index = y * width + new_x
        new_image['pixels'][i] = image['pixels'][new_pixel_index]

    return new_image


def grayscale(image: dict) -> dict:
    width, height = size(image)  # Use size directly
    new_image = create_blank(width, height)

    for i in range(width * height):
        pixel = image['pixels'][i]
        gray = int(pixel[0] * 0.299 + pixel[1] * 0.587 + pixel[2] * 0.114)
        new_image['pixels'][i] = (gray, gray, gray)

    return new_image

def blur(image: dict) -> dict:
    width, height = size(image)
    new_image = create_blank(width, height)

    for i in range(width * height):
        r, g, b = 0, 0, 0
        contador = 0

        # Sumar los píxeles vecinos dentro de la vecindad 3x3
        x = i % width
        y = i // width

        if y > 0:  # Píxel arriba
            r += image['pixels'][i - width][0]
            g += image['pixels'][i - width][1]
            b += image['pixels'][i - width][2]
            contador += 1

        if y < height - 1:  # Píxel abajo
            r += image['pixels'][i + width][0]
            g += image['pixels'][i + width][1]
            b += image['pixels'][i + width][2]
            contador += 1

        if x > 0:  # Píxel izquierda
            r += image['pixels'][i - 1][0]
            g += image['pixels'][i - 1][1]
            b += image['pixels'][i - 1][2]
            contador += 1

        if x < width - 1:  # Píxel derecha
            r += image['pixels'][i + 1][0]
            g += image['pixels'][i + 1][1]
            b += image['pixels'][i + 1][2]
            contador += 1

        # Calcular el promedio
        if contador > 0:
            r //= contador
            g //= contador
            b //= contador

        new_image['pixels'][i] = (r, g, b)

    return new_image


def change_colors(image: dict, original: list[tuple[int, int, int]], change: list[tuple[int, int, int]]) -> dict:
    width, height = size(image)
    new_image = create_blank(width, height)

    for i in range(width * height):
        if image['pixels'][i] in original:
            new_image['pixels'][i] = change[original.index(image['pixels'][i])]
        else:
            new_image['pixels'][i] = image['pixels'][i]
    #images.write_img(image=new_image, filename='imagen_change_colors.png')
    return new_image


def rotate(image: dict, direction: str) -> dict:
    width, height = size(image)
    new_image = create_blank(height, width)

    if direction == "right":
        for y in range(height):
            for x in range(width):
                new_image['pixels'][x * height + (height - y - 1)] = image['pixels'][y * width + x]
    elif direction == "left":
        for y in range(height):
            for x in range(width):

                new_image['pixels'][(width- x - 1) * height + y] = image['pixels'][y * width + x]

    return new_image

def shift(image: dict, horizontal: int, vertical: int) -> dict:
    width, height = size(image)
    new_image = create_blank(width + horizontal, height + vertical) #añadir pixeles negros (tamaño)

    for i in range(width * height):
        x = i % width
        y = i // width
        new_x = x + horizontal
        new_y = y + vertical

        if 0 <= new_x - horizontal < width and 0 <= new_y - vertical < height:
            new_pixel_index = new_y * (width + horizontal) + new_x
            new_image['pixels'][new_pixel_index] = image['pixels'][i]
        else:
            new_image['pixels'][i] = (0, 0, 0)  # Set out-of-bounds pixels to black

    return new_image


def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    width_image, height_image = size(image)
    new_image = create_blank(width, height)

    for i in range(width * height):
        row = i // width
        col = i % width
        new_x = x + col
        new_y = y + row

        if 0 <= new_x < width_image and 0 <= new_y < height_image:
            new_pixel_index = new_y * width_image + new_x
            new_image['pixels'][i] = image['pixels'][new_pixel_index]
        else:
            new_image['pixels'][i] = (0, 0, 0)  # Set out-of-bounds pixels to black

    return new_image

def filter(image: dict, r_factor: float, g_factor: float, b_factor: float) -> dict:
    width, height = size(image)
    new_image = create_blank(width, height)

    for i in range(width * height):
        r, g, b = image['pixels'][i]
        new_r = int(r * r_factor)
        new_g = int(g * g_factor)
        new_b = int(b * b_factor)
        new_image['pixels'][i] = (new_r, new_g, new_b)

    return new_image

