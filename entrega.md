# ENTREGA CONVOCATORIA JUNIO
Luciana Gironda Siña                                            
l.gironda.2023@alumnos.urjc.es

Enlace vídeo: https://youtu.be/V4pcdj_z1Mc

Requisistos mínimos:
- Mirror
- Grayscale
- Blur
- Change_colors
- Rotate
- Shift
- Crop
- Filter
- Transform_simple
- Transform_args
- Transform_multi

Requisistos opcionales:
- Filtros avanzados:                                                
-Tonos sepia       
-Subir o bajar iluminación
